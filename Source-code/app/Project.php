<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded =[];

    public function getRouteKeyName()
    {
        return "id";
    }

    public function path(){
        return route('projects.index');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function projecttypeid()
    {
        return $this->belongsTo(ProjectType::class,'project_type_id');
    }

    public function totalbudget(Project $project)
    {

//        $project = Project::where('id',$id)->first();

            $sum =  $project->firstquarter_budget
                    + $project->secondquarter_budget
                   + $project->thirdquarter_budget
                     + $project->fourthquarter_budget
            ;



       return $sum;
    }



}
