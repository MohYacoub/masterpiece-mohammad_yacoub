<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    public function getRouteKeyName()
    {
        return "id";
    }

    public function path(){
        return route('reporttypes.index');
    }
    protected $guarded =[];
}
