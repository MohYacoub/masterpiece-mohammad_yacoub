<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performanceindicator extends Model
{
    public function getRouteKeyName()
    {
        return "id";
    }

    public function path(){
        return route('performanceindicators.index');
    }
    protected $guarded =[];
}
