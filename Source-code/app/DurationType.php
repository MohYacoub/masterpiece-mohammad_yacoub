<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DurationType extends Model
{
    public function getRouteKeyName()
    {
        return "id";
    }

    public function path(){
        return route('durationtypes.index');
    }
    protected $guarded =[];
}
