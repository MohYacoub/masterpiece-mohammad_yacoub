<?php

namespace App\Http\Controllers;

use App\WorkPlace;
use Illuminate\Http\Request;

class WorkPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workplaces = WorkPlace::latest()->get();
        return view('dashboard_view.workplaces',compact('workplaces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getValidate();
        $workplace = new WorkPlace(\request(['workplace_name', 'city', 'basmacenter']));
        $workplace->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkPlace  $workPlace
     * @return \Illuminate\Http\Response
     */
    public function show(WorkPlace $workplace)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkPlace  $workPlace
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkPlace $workplace)
    {

        return view('dashboard_view.workplaces_edit', compact('workplace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkPlace  $workPlace
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkPlace $workplace)
    {
        $this->getValidate();
        $update = WorkPlace::find($workplace->id);
        $update->workplace_name =   request('workplace_name');
        $update->city =   request('city');
        $update->basmacenter =   request('basmacenter');
        $update->save();
        $massage= $workplace->workplace_name;
//        $workplace->update($this->getValidate());
        return redirect($workplace->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkPlace  $workPlace
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkPlace $workplace)
    {
        $workplace->delete();
//        return redirect()->back()->withErrors('Successfully deleted!');
        return redirect($workplace->path());
    }



    protected function getValidate(): array
    {
        return request()->validate([
            'workplace_name' => 'required',
            'city' => 'required',
            'basmacenter' => 'nullable',
        ]);
    }
}
