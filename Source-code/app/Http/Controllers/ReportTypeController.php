<?php

namespace App\Http\Controllers;

use App\ReportType;
use Illuminate\Http\Request;

class ReportTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reporttypes = ReportType::latest()->get();
        return view('dashboard_view.reporttypes',compact('reporttypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getValidate();
        $reporttype = new ReportType(\request(['name']));
        $reporttype->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReportType  $reportType
     * @return \Illuminate\Http\Response
     */
    public function show(ReportType $reporttype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReportType  $reportType
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportType $reporttype)
    {
        return view('dashboard_view.reporttypes_edit', compact('reporttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportType  $reportType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportType $reporttype)
    {
        $this->getValidate();
        $update = ReportType::find($reporttype->id);
        $update->name =   request('name');
        $update->save();
        $massage= $reporttype->name;
//        $workplace->update($this->getValidate());
        return redirect($reporttype->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReportType  $reportType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportType $reporttype)
    {
        $reporttype->delete();
//        return redirect()->back()->withErrors('Successfully deleted!');
        return redirect($reporttype->path());
    }

    protected function getValidate(): array
    {
        return request()->validate([
            'name' => 'required',
        ]);
    }
}
