<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataEntryProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = auth()->user()->dataentryproject;
        return view('dashboard_view.entryprojects',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projecttypes = ProjectType::latest()->get();
        return view('dashboard_view.projectcreate',compact('projecttypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->getValidate();
        $project = new Project();
        $project->user_id = Auth::id();
        $project->name =$request->input('name');
        $project->project_type_id =$request->input('project_type_id');
        $project->investor =$request->input('investor');
        $project->project_goal =$request->input('project_goal');
        $project->signing_date =$request->input('signing_date');
        $project->starting_date =$request->input('starting_date');
        $project->ending_date =$request->input('ending_date');
        $project->employees =$request->input('employees');
        $project->firstquarter_budget =$request->input('firstquarter_budget');
        $project->secondquarter_budget =$request->input('secondquarter_budget');
        $project->thirdquarter_budget =$request->input('thirdquarter_budget');
        $project->fourthquarter_budget =$request->input('fourthquarter_budget');
        $project->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::where('id',$id)->first();
        return view('dashboard_view.dataentryproject',compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $projecttypes = ProjectType::latest()->get();

        return view('dashboard_view.project_edit', compact('project','projecttypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $this->getValidateupdate($project);
        $update = Project::find($project->id);
        $update->user_id = $update->user_id;
        $update->name =$request->input('name');
        $update->project_type_id =$request->input('project_type_id');
        $update->investor =$request->input('investor');
        $update->project_goal =$request->input('project_goal');
        $update->signing_date =$request->input('signing_date');
        $update->starting_date =$request->input('starting_date');
        $update->ending_date =$request->input('ending_date');
        $update->employees =$request->input('employees');
        $update->firstquarter_budget =$request->input('firstquarter_budget');
        $update->secondquarter_budget =$request->input('secondquarter_budget');
        $update->thirdquarter_budget =$request->input('thirdquarter_budget');
        $update->fourthquarter_budget =$request->input('fourthquarter_budget');
        $update->save();
        $massage= $project->name;
//        $workplace->update($this->getValidate());
        return redirect($project->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    protected function getValidate(): array
    {
        return request()->validate([
            'name' => 'required',
            'investor' => 'required',
            'project_type_id' => 'required',
            'project_goal' => 'required',
            'signing_date' => 'required|date',
            'starting_date' => 'required|date',
            'ending_date' => 'required|date|after:starting_date',
            'employees' => 'required|integer',
            'firstquarter_budget' => 'required|integer',
            'secondquarter_budget' => 'required|integer',
            'thirdquarter_budget' => 'required|integer',
            'fourthquarter_budget' => 'required|integer',
        ]);
    }


  protected function getValidateupdate(): array
    {
        return request()->validate([
            'name' => 'required',
            'investor' => 'required',
            'project_type_id' => 'required',
            'project_goal' => 'required',
            'signing_date' => 'required|date',
            'starting_date' => 'required|date',
            'ending_date' => 'required|date|after:starting_date',
            'employees' => 'required|integer',
            'firstquarter_budget' => 'required|integer',
            'secondquarter_budget' => 'required|integer',
            'thirdquarter_budget' => 'required|integer',
            'fourthquarter_budget' => 'required|integer',
        ]);
    }

}
