<?php

namespace App\Http\Controllers;

use App\DurationType;
use Illuminate\Http\Request;

class DurationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $durationtypes = DurationType::latest()->get();
        return view('dashboard_view.durationtypes',compact('durationtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getValidate();
        $durationtype = new DurationType(\request(['name','days']));
        $durationtype->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DurationType  $durationType
     * @return \Illuminate\Http\Response
     */
    public function show(DurationType $durationtype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DurationType  $durationType
     * @return \Illuminate\Http\Response
     */
    public function edit(DurationType $durationtype)
    {
        return view('dashboard_view.durationtypes_edit', compact('durationtype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DurationType  $durationType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DurationType $durationtype)
    {
        $this->getValidate();
        $update = DurationType::find($durationtype->id);
        $update->name =   request('name');
        $update->days =   request('days');
        $update->save();
        $massage= $durationtype->name;
//        $workplace->update($this->getValidate());
        return redirect($durationtype->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DurationType  $durationType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DurationType $durationtype)
    {
        $durationtype->delete();
//        return redirect()->back()->withErrors('Successfully deleted!');
        return redirect($durationtype->path());
    }

    protected function getValidate(): array
    {
        return request()->validate([
            'name' => 'required',
            'days'=>'required',
        ]);
    }
}
