<?php

namespace App\Http\Controllers;

use App\Performanceindicator;
use Illuminate\Http\Request;

class PerformanceindicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $performanceindicators = Performanceindicator::latest()->get();
        return view('dashboard_view.performanceindicators',compact('performanceindicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getValidate();
        $performanceindicator = new Performanceindicator(\request(['name']));
        $performanceindicator->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Performanceindicator  $performanceindicator
     * @return \Illuminate\Http\Response
     */
    public function show(Performanceindicator $performanceindicator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Performanceindicator  $performanceindicator
     * @return \Illuminate\Http\Response
     */
    public function edit(Performanceindicator $performanceindicator)
    {
        return view('dashboard_view.performanceindicators_edit', compact('performanceindicator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Performanceindicator  $performanceindicator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Performanceindicator $performanceindicator)
    {
        $this->getValidate();
        $update = Performanceindicator::find($performanceindicator->id);
        $update->name =   request('name');
        $update->save();
        $massage= $performanceindicator->name;
//        $workplace->update($this->getValidate());
        return redirect($performanceindicator->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Performanceindicator  $performanceindicator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Performanceindicator $performanceindicator)
    {
        $performanceindicator->delete();
//        return redirect()->back()->withErrors('Successfully deleted!');
        return redirect($performanceindicator->path());
    }

    protected function getValidate(): array
    {
        return request()->validate([
            'name' => 'required',
        ]);
    }
}
