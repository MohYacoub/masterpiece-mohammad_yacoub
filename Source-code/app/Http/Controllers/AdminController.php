<?php

namespace App\Http\Controllers;

use App\User;
use App\WorkPlace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Rules\FullName;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workplaces = WorkPlace::latest()->get();
        $admins = User::where("role", 2)->get();
        return view('dashboard_view.admincreate',compact('workplaces','admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd("fdsafds");
        $this->getValidate();
        $user = new User();
        $user->name =$request->input('name');
        $user->email =$request->input('email');
        $user->password =Hash::make($request->input('password'));
        $user->role =2;
        $user->mobile =$request->input('mobile');
        $user->job_number = $request->input('job_number');
        $user->work_place_id =$request->input('work_place_id');
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {
        $workplaces = WorkPlace::latest()->get();
        return view('dashboard_view.admins_edit', compact('admin','workplaces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin)
    {
        $this->getValidateupdate($admin);
        $update = User::find($admin->id);
        $update->name =   request('name');
        $update->email =   request('email');

        $update->password =Hash::make(request('password'));
        $update->role =2;
        $update->mobile =request('mobile');
        $update->job_number = request('job_number');
        $update->work_place_id =request('work_place_id');
        $update->save();
        $massage= $admin->name;
//        $workplace->update($this->getValidate());
        return redirect($admin->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $admin)
    {
        $admin->delete();
//        return redirect()->back()->withErrors('Successfully deleted!');
        return redirect($admin->path());
    }


    protected function getValidate(): array
    {
        return request()->validate([
            'name' => ['required', new FullName()],
            'email' => 'required|email|unique:users,email,$this->id,id',
            'mobile' => 'required|unique:users,mobile,$this->id,id|digits:14',
            'job_number' => 'required','min:1','max:5',
            'password' => ['required','confirmed','string','min:8','regex:/[A-Za-z]/',
                'regex:/[0-9]/',
            ],
            'work_place_id' => 'required'
        ]);
    }

    protected function getValidateupdate($admin): array
    {
        return request()->validate([
            'name' => ['required', new FullName()],
            'email' => 'required|unique:users,email,'.$admin->id.',',
            'mobile' => 'digits:14,required|unique:users,mobile,'.$admin->id.',',
            'job_number' => 'required','min:1','max:5',
            'password' => ['required','confirmed','string','min:8','regex:/[A-Za-z]/',
                'regex:/[0-9]/',
            ],
            'work_place_id' => 'required'
        ]);
    }

}
