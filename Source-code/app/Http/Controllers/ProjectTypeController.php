<?php

namespace App\Http\Controllers;

use App\ProjectType;
use Illuminate\Http\Request;

class ProjectTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projecttypes = ProjectType::latest()->get();
        return view('dashboard_view.projecttypes',compact('projecttypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getValidate();
        $projecttype = new ProjectType(\request(['name']));
        $projecttype->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectType $projecttype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectType $projecttype)
    {
        return view('dashboard_view.projecttypes_edit', compact('projecttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectType $projecttype)
    {
        $this->getValidate();
        $update = ProjectType::find($projecttype->id);
        $update->name =   request('name');
        $update->save();
        $massage= $projecttype->name;
//        $workplace->update($this->getValidate());
        return redirect($projecttype->path())->with('massage',$massage);
//        return redirect()->route('workplaces.index', [$massage]);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectType $projecttype)
    {
        $projecttype->delete();
//        return redirect()->back()->withErrors('Successfully deleted!');
        return redirect($projecttype->path());
    }


    protected function getValidate(): array
    {
        return request()->validate([
            'name' => 'required',
        ]);
    }
}
