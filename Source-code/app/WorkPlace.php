<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkPlace extends Model
{
    public function getRouteKeyName()
    {
        return "id";
    }

    public function path(){
        return route('workplaces.index');
    }
    protected $guarded =[];

    public function users()
    {
        return $this->hasMany(User::class,'work_place_id');
    }
}
