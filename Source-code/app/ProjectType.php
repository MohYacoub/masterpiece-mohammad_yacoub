<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    public function getRouteKeyName()
    {
        return "id";
    }

    public function path(){
        return route('projecttypes.index');
    }
    protected $guarded =[];


    public function projecttypes(){
        return $this->hasMany(ProjectType::class,'project_type_id');
    }

}
