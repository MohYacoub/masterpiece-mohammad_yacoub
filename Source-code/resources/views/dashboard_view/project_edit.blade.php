
@extends('dashboard_layouts.main')


@section('content')


    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

				<span class="ribbon-button-alignment">
			 		<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"
                          rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Home</li>
                <li>DataEntry</li>
                <li>Projects</li>
            </ol>
            <!-- end breadcrumb -->

            <!-- You can also add more buttons to the
            ribbon for further usability

            Example below:

            <span class="ribbon-button-alignment pull-right">
            <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
            <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
            <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
            </span> -->

        </div>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">

            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> DataEntry <span>>
                            Projects</span></h1>
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <!-- Button trigger modal -->
                    <a data-toggle="modal" href="#myModal" class="btn btn-success btn-md pull-right header-btn
                    hidden-mobile"><i class="fa fa-backward"></i> back</a>
                </div>




            </div>
        @if (session('massage'))
            <!-- widget grid -->
            <div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Edited Done!</h4>
            </div>
            @endif


            <!-- widget grid -->
            <section id="widget-grid" class="">

                <!-- row -->

                <div class="row">


                    <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget jarviswidget-sortable" id="wid-id-88" data-widget-colorbutton="false"
                             data-widget-editbutton="false" role="widget" style="">
                            <!-- widget options:
                            usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                            data-widget-colorbutton="false"
                            data-widget-editbutton="false"
                            data-widget-togglebutton="false"
                            data-widget-deletebutton="false"
                            data-widget-fullscreenbutton="false"
                            data-widget-custombutton="false"
                            data-widget-collapsed="true"
                            data-widget-sortable="false"

                            -->
                             <header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a
                                          href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn"
                                           rel="tooltip" title="" data-placement="bottom"
                                           data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a
                                          href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn"
                                           rel="tooltip" title="" data-placement="bottom"
                                           data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a
                                          href="javascript:void(0);" class="button-icon jarviswidget-delete-btn"
                                           rel="tooltip" title="" data-placement="bottom"
                                          data-original-title="Delete"><i class="fa fa-times"></i></a></div>
                                 <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                                <h2>Project Form</h2>

                                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                            <!-- widget div-->
                            <div role="content">

                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body">

                                    <div class="alert alert-danger print-error-msg" style="display:none">

                                        <ul></ul>

                                    </div>

                                    <form id="form" action="{{ route('projects.update',$project->id)}}" method="post"
                                          class="form-horizontal">
                                        @method('put')
                                       @csrf
                                        <fieldset>
                                            <legend>Create Project</legend>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">Project Name</label>
                                                <div class="col-md-9">
                                                    <input id="name" name="name" class="form-control"
                                                           placeholder="Project Name" type="text"
                                                           value="{{$project->name}}">
                                                    @error('name')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">Project investor</label>
                                                <div class="col-md-9">
                                                    <input id="investor" name="investor" class="form-control"
                                                           placeholder="Investor" type="text" value="{{$project->investor}}">
                                                    @error('investor')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label" for="select-1">Select
                                                    Project Type</label>
                                                <div class="col-md-9">

                                                    <select name="project_type_id" class="form-control" id="project_type_id">
                                                        <option value="{{$project->projecttypeid->id}}"
                                                            {{ $project->projecttypeid->id == $project->projecttypeid->id ?
                                                            'selected' : ''}}>
                                                            {{ $project->projecttypeid->name }}
                                                        </option>

                                                        @foreach($projecttypes as $projecttype)
                                                        <option value="{{$projecttype->id}}">{{$projecttype->name}}</option>
                                                        @endforeach
                                                    </select>

                                                    @error('project_type_id')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror

                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">Signing Date</label>
                                                <div class="col-md-9">
                                                    <input data-dateformat="dd/mm/yy" id="signing_date" name="signing_date"
                                                           class="form-control"
                                                           placeholder="signing date" type="date" value="{{$project->signing_date}}">
                                                    @error('signing_date')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">Project Goal</label>
                                                <div class="col-md-9">
                                                    <textarea name="project_goal" class="form-control"
                                                              placeholder="project Goal"
                                                               rows="6">{{$project->project_goal}}</textarea>
                                                </div>
                                            </div>


                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">Starting Date</label>
                                                <div class="col-md-9">
                                                    <input data-dateformat="dd/mm/yy" id="starting_date" name="starting_date"
                                                           class="form-control"
                                                           placeholder="starting date" type="date" value="{{$project->starting_date}}">
                                                    @error('starting_date')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">Ending Date</label>
                                                <div class="col-md-9">
                                                    <input data-dateformat="dd/mm/yy" id="ending_date" name="ending_date"
                                                           class="form-control"
                                                           placeholder="Ending date" type="date" value="{{$project->ending_date}}">
                                                    @error('ending_date')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">number of employees</label>
                                                <div class="col-md-9">
                                                    <input id="employees" name="employees" class="form-control"
                                                           placeholder="number of employees" type="text" value="{{$project->employees}}">
                                                    @error('investor')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">1st quarter Badget</label>
                                                <div class="col-md-9">
                                                    <input id="firstquarter_budget" name="firstquarter_budget" class="form-control"
                                                           placeholder="1st quarter Badget" type="text" value="{{$project->firstquarter_budget}}">
                                                    @error('firstquarter_budget')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">2nd quarter Badget</label>
                                                <div class="col-md-9">
                                                    <input id="secondquarter_budget" name="secondquarter_budget" class="form-control"
                                                           placeholder="2nd quarter Badget" type="text" value="{{$project->secondquarter_budget}}">
                                                    @error('secondquarter_budget')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">3rd quarter Badget</label>
                                                <div class="col-md-9">
                                                    <input id="thirdquarter_budget" name="thirdquarter_budget" class="form-control"
                                                           placeholder="3rd quarter Badget" type="text" value="{{$project->thirdquarter_budget}}">
                                                    @error('thirdquarter_budget')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label class="col-md-3 control-label">4th quarter Badget</label>
                                                <div class="col-md-9">
                                                    <input id="fourthquarter_budget" name="fourthquarter_budget" class="form-control"
                                                           placeholder="4th quarter Badget" type="text" value="{{$project->fourthquarter_budget}}">
                                                    @error('thirdquarter_budget')
                                                    <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>


                                        </fieldset>


                                        <div class="form-actions" >
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="fa fa-save"></i>
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->

                        </div>
                        <!-- end widget -->

                    </article>

                </div>

                <!-- end row -->



            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->

@endsection

@section('page_script')


    <script src="{{ asset('dashboard_theme/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>


    <script type="text/javascript">

        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        $(document).ready(function() {

            pageSetUp();

            /* // DOM Position key index //

            l - Length changing (dropdown)
            f - Filtering input (search)
            t - The Table! (datatable)
            i - Information (records)
            p - Pagination (paging)
            r - pRocessing
            < and > - div elements
            <"#id" and > - div with an id
            <"class" and > - div with a class
            <"#id.class" and > - div with an id and class

            Also see: http://legacy.datatables.net/usage/features
            */

            /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });

            /* END BASIC */

            /* COLUMN FILTER  */
            var otable = $('#datatable_fixed_column').DataTable({
                //"bFilter": false,
                //"bInfo": false,
                //"bLengthChange": false
                //"bAutoWidth": false,
                //"bPaginate": false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_fixed_column) {
                        responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#datatable_fixed_column'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_fixed_column.respond();
                }

            });

            // custom toolbar
            $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

            // Apply the filter
            $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

                otable
                    .column( $(this).parent().index()+':visible' )
                    .search( this.value )
                    .draw();

            } );
            /* END COLUMN FILTER */

            /* COLUMN SHOW - HIDE */
            $('#datatable_col_reorder').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            /* END COLUMN SHOW - HIDE */

            /* TABLETOOLS */
            $('#datatable_tabletools').dataTable({

                // Tabletools options:
                //   https://datatables.net/extensions/tabletools/button_options
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "oTableTools": {
                    "aButtons": [
                        "copy",
                        "csv",
                        "xls",
                        {
                            "sExtends": "pdf",
                            "sTitle": "SmartAdmin_PDF",
                            "sPdfMessage": "SmartAdmin PDF Export",
                            "sPdfSize": "letter"
                        },
                        {
                            "sExtends": "print",
                            "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
                        }
                    ],
                    "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
                },
                "autoWidth" : true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_tabletools) {
                        responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_tabletools.respond();
                }
            });

            /* END TABLETOOLS */

        })

    </script>

    <script type="text/javascript">
        $(document).ready(function (){
            $("#form").on('submit', function (event){
                event.preventDefault();
                        Swal.fire({
                            title: 'Are you sure?',
                            text: "You won't be save this!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, save it!'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: "POST",
                                    url: "/dataentry/projects",
                                    data: $("#form").serialize(),
                                    success: function (response){
                                Swal.fire(
                                    'Saved!',
                                    'Your Project has been Recorded.',
                                    'success'
                                );
                                        // location.reload(true);
                                        console.log("Fdsfsd");
                            },
                            error: function (error){
                                printErrorMsg(error.responseJSON.errors);
                                console.log(error)
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Please Enter Correct Data!',
                                })
                            }});
                    };

                });
            });
        });

        function printErrorMsg (msg) {

            $(".print-error-msg").find("ul").html('');

            $(".print-error-msg").css('display','block');

            $.each( msg, function( key, value ) {

                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

            });

        }

    </script>


@endsection
