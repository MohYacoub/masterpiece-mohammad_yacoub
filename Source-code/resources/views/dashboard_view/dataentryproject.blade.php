
@extends('dashboard_layouts.main')


@section('content')


    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

				<span class="ribbon-button-alignment">
			 	<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"
                       rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa
                       fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Dashboard</li><li>Projects</li><li>Project Detail</li>
            </ol>
            <!-- end breadcrumb -->

            <!-- You can also add more buttons to the
            ribbon for further usability

            Example below:

            <span class="ribbon-button-alignment pull-right">
            <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
            <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
            <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
            </span> -->

        </div>
        <!-- END RIBBON -->



        <!-- MAIN CONTENT -->
        <div id="content">

            <!-- row -->
            <div class="row">

                <!-- col -->
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <h1 class="page-title txt-color-blueDark">

                        <!-- PAGE HEADER -->
                        <i class="fa-fw fa fa-home"></i>
                        Projects
                        <span>>
								Project View
							</span>
                    </h1>
                </div>
                <!-- end col -->

                <!-- right side of the page with the sparkline graphs -->
                <!-- col -->
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8 text-right">

                    <a href="{{route('projects.edit',$project->id)}}" class="btn btn-default shop-btn">
                        <i class="fa fa-3x fa-edit"></i>
                    </a>
                    <a href="javascript:void(0);" class="btn btn-default">
                        <i class="fa fa-3x fa-print"></i>
                    </a>

                </div>
                <!-- end col -->

            </div>
            <!-- end row -->

            <!--
                The ID "widget-grid" will start to initialize all widgets below
                You do not need to use widgets if you dont want to. Simply remove
                the <section></section> and you can use wells or panels instead
                -->

            <!-- widget grid -->
            <section id="widget-grid" class="">

                <!-- row -->

                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <!-- product -->
                        <div class="product-content product-wrap clearfix product-deatil">
                            <div class="row">

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="btn-group pull-right">
                                        @if($project->status == 0)
                                        <span class="label btn-lg label-warning">Pending</span>
                                        @elseif($project->status == 1)
                                            <span class="label btn-lg label-success">Acepted</span>
                                        @else
                                            <span class="label btn-lg label-danger">Rejected</span>
                                        @endif
                                    </div>
                                    <h2 class="name">
                                        {{$project->name}}
                                         <small>create by <a href="javascript:void(0);">{{$project->users->name}}</a></small>
                                    </h2>



                                    <hr>
                                    <strong>Badget Details</strong>
                                    <div class="certified">
                                        <ul>
                                            <li><a href="javascript:void(0);">1st quarter
                                                    Badget<span>{{$project->firstquarter_budget}} JD</span></a></li>
                                            <li><a href="javascript:void(0);">2nd quarter
                                                    Badget<span>{{$project->secondquarter_budget}} JD</span></a></li>
                                            <li><a href="javascript:void(0);">3rd quarter
                                                    Badget<span>{{$project->thirdquarter_budget}} JD</span></a></li>
                                            <li><a href="javascript:void(0);">4th quarter
                                                    Badget<span>{{$project->fourthquarter_budget}} JD</span></a></li>
                                        </ul>
                                    </div>

                                    <h3 class="price-container">

                                        Total Budget = {{$project->totalbudget($project)}} JD
                                        {{--                                        <small>*includes tax</small>--}}
                                    </h3>
                                    <hr>
                                    <div class="description description-tabs">


                                        <ul id="myTab" class="nav nav-pills">
                                            <li class="active"><a href="#more-information" data-toggle="tab"
                                                                  class="no-margin">Project Description </a></li>
                                            <li class=""><a href="#specifications" data-toggle="tab">Employees</a></li>
                                            <li class=""><a href="#specifications2" data-toggle="tab">Employees</a></li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content">
                                            <div class="tab-pane fade active in" id="more-information">
                                                <br>
                                                <strong>Investor</strong>
                                                <p>{{$project->investor}}</p>
                                                <strong>Project Goal</strong>
                                                 <p>{{$project->project_goal}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="specifications">
                                                <br>
                                                <dl class="">
                                                    <dt>Gravina</dt>
                                                    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                                    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                                    <dd>Eget lacinia odio sem nec elit.</dd>
                                                    <br>

                                                    <dt>Test lists</dt>
                                                    <dd>A description list is perfect for defining terms.</dd>
                                                    <br>

                                                    <dt>Altra porta</dt>
                                                    <dd>Vestibulum id ligula porta felis euismod semper</dd>
                                                </dl>
                                            </div>
                                            <div class="tab-pane fade" id="specifications2">
                                                <br>
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th> <i class="fa fa-building"></i> Product</th>
                                                        <th> <i class="fa fa-calendar"></i> Payment Taken</th>
                                                        <th> <i class="glyphicon glyphicon-send"></i> Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="success">
                                                        <td>1</td>
                                                        <td>TB - Monthly</td>
                                                        <td>01/04/2012</td>
                                                        <td>Approved</td>
                                                    </tr>
                                                    <tr class="danger">
                                                        <td>2</td>
                                                        <td>TB - Monthly</td>
                                                        <td>02/04/2012</td>
                                                        <td>Declined</td>
                                                    </tr>
                                                    <tr class="warning">
                                                        <td>3</td>
                                                        <td>TB - Monthly</td>
                                                        <td>03/04/2012</td>
                                                        <td>Pending</td>
                                                    </tr>
                                                    <tr class="info">
                                                        <td>4</td>
                                                        <td>TB - Monthly</td>
                                                        <td>04/04/2012</td>
                                                        <td>Call in to confirm</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6 col-lg-6">

                                            <a href="javascript:void(0);" class="btn btn-success btn-lg">Accept</a>
                                            <a href="javascript:void(0);" class="btn btn-danger btn-lg">Reject</a>
{{--                                            <a href="javascript:void(0);" class="btn btn-success btn-lg">Add to cart</a>--}}

                                        </div>
                                        <div class="btn-group pull-right">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end product -->
                    </div>



                </div>

                <!-- end row -->

            </section>
            <!-- end widget grid -->

        </div>
        <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->

@endsection

@section('page_script')


    <script src="{{ asset('dashboard_theme/js/plugin/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatables/dataTables.colVis.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatables/dataTables.tableTools.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard_theme/js/plugin/datatable-responsive/datatables.responsive.min.js')}}"></script>


    <script type="text/javascript">

        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        $(document).ready(function() {

            pageSetUp();

            /* // DOM Position key index //

            l - Length changing (dropdown)
            f - Filtering input (search)
            t - The Table! (datatable)
            i - Information (records)
            p - Pagination (paging)
            r - pRocessing
            < and > - div elements
            <"#id" and > - div with an id
            <"class" and > - div with a class
            <"#id.class" and > - div with an id and class

            Also see: http://legacy.datatables.net/usage/features
            */

            /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

            $('#dt_basic').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_dt_basic.respond();
                }
            });

            /* END BASIC */

            /* COLUMN FILTER  */
            var otable = $('#datatable_fixed_column').DataTable({
                //"bFilter": false,
                //"bInfo": false,
                //"bLengthChange": false
                //"bAutoWidth": false,
                //"bPaginate": false,
                //"bStateSave": true // saves sort state using localStorage
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                     if (!responsiveHelper_datatable_fixed_column) {
                         responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($
                         ('#datatable_fixed_column'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_fixed_column.respond();
                }

            });

            // custom toolbar
            $("div.toolbar").html('<div class="text-right">' +
                '<img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

            // Apply the filter
            $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {

                otable
                    .column( $(this).parent().index()+':visible' )
                    .search( this.value )
                    .draw();

            } );
            /* END COLUMN FILTER */

            /* COLUMN SHOW - HIDE */
            $('#datatable_col_reorder').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            /* END COLUMN SHOW - HIDE */

            /* TABLETOOLS */
            $('#datatable_tabletools').dataTable({

                // Tabletools options:
                //   https://datatables.net/extensions/tabletools/button_options
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "oTableTools": {
                    "aButtons": [
                        "copy",
                        "csv",
                        "xls",
                        {
                            "sExtends": "pdf",
                            "sTitle": "SmartAdmin_PDF",
                            "sPdfMessage": "SmartAdmin PDF Export",
                            "sPdfSize": "letter"
                        },
                        {
                            "sExtends": "print",
                            "sMessage": "Generated by SmartAdmin <i>(press Esc to close)</i>"
                        }
                    ],
                    "sSwfPath": "{{ asset('dashboard_theme/js/plugin/datatables/swf/copy_csv_xls_pdf.swf')}}"
                },
                "autoWidth" : true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_tabletools) {
                         responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($
                         ('#datatable_tabletools'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_tabletools.respond();
                }
            });

            /* END TABLETOOLS */

        })

    </script>



@endsection
