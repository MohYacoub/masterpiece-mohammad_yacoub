<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int
            (1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,
                9).random_int(1,9).random_int(1,9),
        'work_place_id' => '1',
        'role' => '3',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'job_number'=>random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9),
    ];
});

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int
            (1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,
                9).random_int(1,9).random_int(1,9),
        'work_place_id' => '1',
        'role' => '2',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'job_number'=>random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9).random_int(1,9),
    ];
});
