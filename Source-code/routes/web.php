<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard_view.dashboard');
});

Route::get('/dataentry/projects', function () {
    return view('dashboard_view.entryprojects');
});

//Route::get('/dataentry/projects/create', function () {
//    return view('dashboard_view.projectcreate');
//});


//Route::get('/dashboard/admin','AdminController@create');
//Route::post('dashboard/workplaces/{workplace}','WorkPlaceController@destroy');

Route::resource('/dashboard/workplaces', WorkPlaceController::class);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/dashboard/admins', AdminController::class);
Route::resource('/dashboard/dataentry', DataEntryController::class);
Route::resource('/dashboard/projecttypes', ProjectTypeController::class);
Route::resource('/dashboard/reporttypes', ReportTypeController::class);
Route::resource('/dashboard/durationtypes', DurationTypeController::class);
Route::resource('/dashboard/performanceindicators', PerformanceindicatorController::class);


Route::resource('/dataentry/projects', DataEntryProjectController::class);

Route::get('/dataentry/projectssd', function () {
    return view('dashboard_view.dataentryproject');
});


